import React, {useState} from 'react'
import DataGrid from 'react-data-grid'
import './App.css'

type Equipment = {
    id: number
    title: string
}
type Project = {
    id: number
    title: string
    equipment_id_list: number[]
}

function genDefaultEquipments(): Equipment[] {
    let rows: Equipment[] = []
    for (let i = 0; i < 100; i++) {
        rows[i] = {id: i + 1, title: 'Equip ' + (i + 1)}
    }
    return rows
}

function genDefaultProjects(): Project[] {
    let rows: Project[] = []
    for (let i = 0; i < 100; i++) {
        rows[i] = {
            id: i + 1, title: 'project ' + (i + 1),
            equipment_id_list: [3,4,5],
        }
    }
    return rows
}

function ProjectList(props: {
    all_project: Project[]
    all_equipment: Equipment[]
}) {
    const {all_equipment, all_project} = props
    const rows = all_project

    const [selected_project, set_selected_project] = useState<Project | null>(null)

    function renderChoose({row}: { row: Project }) {
        return <button onClick={() => set_selected_project(row)}>choose</button>
    }

    const columns = [
        {
            key: 'choose',
            name: 'Choose',
            formatter: renderChoose,
        },
        {key: 'id', name: 'ID'},
        {key: 'title', name: 'Title'},
    ]
    const rowsCount = rows.length
    const rowGetter = (i: number) => rows[i]
    if (selected_project) {
        return <div>
            <h2>Project Detail: {selected_project.title}</h2>
            <button onClick={() => set_selected_project(null)}>cancel</button>
            <EquipmentList
                key={selected_project.id}
                all_equipment={all_equipment} project={selected_project}/>
        </div>
    }
    return <div>
        <h2>Project List</h2>
        <DataGrid
            key={'project'}
            columns={columns}
            rowsCount={rowsCount}
            rowGetter={rowGetter}
            minHeight={500}
        />
        <p>{JSON.stringify(rows)}</p>
    </div>
}

function EquipmentList(props: {
    project: Project,
    all_equipment: Equipment[]
}) {
    const rows = props.all_equipment
    const columns = [
        {
            key: 'checked',
            name: 'Checked',
            formatter: renderChecked,
        },
        {key: 'id', name: 'ID'},
        {key: 'title', name: 'Title'},
    ]

    const [checked_id_list, set_checked_id_list] = useState(props.project.equipment_id_list)

    function renderChecked(rowObject: any) {
        console.log({rowObject})
        const {checked,...equipment } = rowObject.row
        let row = equipment
        console.log('render checked:', {id: row.id, checked})
        return <input
            key={row.id}
            type="checkbox"
            checked={checked}
            onChange={e => {
                let checked = e.currentTarget.checked
                console.log('checked:', {checked, id: row.id})
                set_checked_id_list(id_list => {
                    if (checked) {
                        return [...id_list, row.id]
                    } else {
                        return id_list.filter(id => id !== row.id)
                    }
                })
            }}
        />
    }

    const rowsCount = rows.length
    const rowGetter = (i: number) => {
        let row = rows[i]
        if (!row) {
            return
        }
        console.log('rowGetter', {i, row})
        let checked = checked_id_list.includes(row.id)
        return {...row, checked}
    }


    return (
        <div className="App">
            <DataGrid
                key={'equipment'}
                columns={columns}
                rowsCount={rowsCount}
                rowGetter={rowGetter}
                minHeight={500}
            />
            <p>{JSON.stringify({project_id:props.project.id,checked_id_list})}</p>
            {/*<button onClick={selectAll}>select all</button>*/}
            {/*<button onClick={unselectAll}>unselect all</button>*/}
            {/*<button onClick={save}>save</button>*/}
        </div>
    )
}

function App() {
    const [all_project, set_all_project] = useState(genDefaultProjects)
    const [all_equipment, set_all_equipment] = useState(genDefaultEquipments)
    return <div>
        <ProjectList all_project={all_project} all_equipment={all_equipment}/>
    </div>
}

export default App
